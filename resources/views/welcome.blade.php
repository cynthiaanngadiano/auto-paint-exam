<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
				<title>Auto Paint Exam</title>
	<style>
		body {
			margin: 0;
			padding: 0;
		}
		header {
			padding: 20px;
			text-align: center;
			background-color: #eeeeee;
			color: #656565;
			font-family: arial;
			font-size: 20px;
		}
		ul {
			list-style-type: none;
			margin: 0;
			padding: 0;
			overflow: hidden;
			background-color: #ea6a5b;
		}

		li {
			float: left;
		}

		li a {
			font-family: arial;
			font-weight: bold;
			display: block;
			color: white;
			text-align: center;
			padding: 14px 16px;
			text-decoration: none;
		}

		li a:hover:not(.active) {
			background-color: #111;
		}

		.active {
			color: black;
		}
		
		#newpaint {
			font-family: arial;
			font-size: 28px;
			text-align: center;
			padding-top: 15px;
		}
		
		#default-car {
			float: left;
			padding-left: 200px;
			width: 320px;
			height: 250px;
		}
		
		#arrow {
			padding-top: 65px;
			padding-left: 90px;
		}
		
		#new-car {
			float: right;
			padding-right: 200px;
			width: 320px;
			height: 250px;
		}
		
		form {
			margin-top: 100px;
			font-family: arial;
			padding-left: 50px;
		}
		
		select {
			color: gray;
			width: 200px;
			font-size: 17px;
		}
		
		option {
			color: black;
		}
		
		.button {
			background-color: #ea6a5b;
			border: none;
			color: white;
			padding: 8px 70px;
			text-align: center;
			text-decoration: none;
			display: inline-block;
			font-size: 16px;
			cursor: pointer;
			margin: 15px 6px;
		}
	</style>
	<script language="Javascript">
		function changePic(id,loc,newimg) {
			document.getElementById(id).src ="file:///C:/Users/DELL/Desktop/AutoPaint" + newimg + ".jpg";
		}
	</script>
	</head>
	<body>
		<header>
			<h1>JUAN'S AUTO PAINT<h2>
		</header>
		<nav>
			<ul>
			  <li><a class="active" href="#new">NEW PAINT JOB</a></li>
			  <li><a href="#jobs">PAINT JOBS</a></li>
			</ul>
		</nav>
		<div id="cars">
			<h1 id="newpaint">New Paint Job</h1><br><br>
			<img id="default-car" src="../assets/images/Default.jpg" alt="Default Car Color">
			<img id="arrow" src="../assets/images/arrow.jpg" alt="Turn car color into">
			<img id="new-car" src="../assets/images/Default.jpg" alt="New Car Color">
			<br><br>
			<form>
				<h3 id="details">Car Details</h3>
			<table cellpadding="3">
			<tr><td>Plate No.</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="plate" style="font-size: 16px;"></td></tr>
			<tr><td>Current Color</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<select name="default" onChange="changePic('default-car','images',this.value)">
					  <option value="Default" readonly></option>
					  <option value="Red">Red</option>
					  <option value="Green">Green</option>
					  <option value="Blue">Blue</option>
					</select></td></tr>
			<tr><td>Target Color</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<select name="new" onChange="changePic('new-car','images',this.value)">
					  <option value="Default" readonly></option>
					  <option value="Red">Red</option>
					  <option value="Green">Green</option>
					  <option value="Blue">Blue</option>
					</select></td></tr>
			</table>
			<button class="button">Submit</button>
			</form><br><br><br><br><br>

		</div>
	</body>
</html>